import sys
import os
if sys.version < '3':
    from .btcommon import *
else:
    from bluetooth.btcommon import *

__version__ = 0.22

def _dbg(*args):
    return
    sys.stderr.write(*args)
    sys.stderr.write("\n")

if sys.platform == "win32":
    _dbg("trying widcomm")
    have_widcomm = False
    dll = "wbtapi.dll"
    sysroot = os.getenv ("SystemRoot")
    if os.path.exists (dll) or \
       os.path.exists (os.path.join (sysroot, "system32", dll)) or \
       os.path.exists (os.path.join (sysroot, dll)):
        try:
            from . import widcomm
            if widcomm.inquirer.is_device_ready ():
                # if the Widcomm stack is active and a Bluetooth device on that
                # stack is detected, then use the Widcomm stack
                from .widcomm import *
                have_widcomm = True
        except ImportError: 
            pass

    if not have_widcomm:
        # otherwise, fall back to the Microsoft stack
        _dbg("Widcomm not ready. falling back to MS stack")
        if sys.version < '3':
            from .msbt import *
        else:
            from bluetooth.msbt import *

elif sys.platform.startswith("linux"):
    if sys.version < '3':
        from .bluez import *
    else:
        from bluetooth.bluez import *
elif sys.platform == "darwin":
    from .osx import *
else:
    raise Exception("This platform (%s) is currently not supported by pybluez." % sys.platform)

discover_devices.__doc__ = \


lookup_name.__doc__ = \
advertise_service.__doc__ = \

stop_advertising.__doc__ = \

find_service.__doc__ = \

