#ifndef __btmodule_h__
#define __btmodule_h__

#include "Python.h"
#include <bluetooth/bluetooth.h>
#include <bluetooth/sdp.h>
#include <bluetooth/sdp_lib.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	PyObject_HEAD
	int sock_fd;	
	int sock_family;	
	int sock_type;		
	int sock_proto;		
	PyObject *(*errorhandler)(void); /
	double sock_timeout;		 

    int is_listening_socket;  

    uint32_t sdp_record_handle; 
    sdp_session_t *sdp_session;
} PySocketSockObject;


#ifdef __cplusplus
}
#endif

extern PyObject *bluetooth_error;

#endif 
