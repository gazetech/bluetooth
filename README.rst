
**INSTALLATION:**

Use pip
    
    pip install pybluez

For experimental use of Bluetooth Low Energy support:

    pip install pybluez[ble]

For source installation:

    python setup.py install

for Bluetooth Low Energy support:

    pip install -e .[ble]


